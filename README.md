# Installation instructions
This project use docker for the development environment, so you can start the server with the following command:
```bash
docker-compose up -d
```

Use the following command to go into the container:
```bash
docker exec -it -w /app TeamBlue bash
```

After that, You need to install dependencies with the following command:
```bash
composer install
```
# Usage

You can run the CLI tool with the following command:
```bash
php bin/urlhasher
```
### Arguments
- `URLs` You can use this argument as an array to specify one or multiple remote files (Separate multiple URLs with a space).

Example:
```bash
php bin/urlhasher http://speed.transip.nl/10mb.bin http://speed.transip.nl/100mb.bin
```

### Options
- `--no-cache` You can use this option to disable downloader caching.

Default is `false`

Example:
```bash
php bin/urlhasher http://speed.transip.nl/10mb.bin --no-cache
```

- `--algo` You can use this option to select hashing algorithm.

Default algorithm is `md5`

> **NOTE:** This CLI tool support all the standard PHP algorithm provided in the `hash_algos()` method. <br>
> You can also implement your custom algorithm in `core/FileHasher/Algorithms/`

Standard algorithm example:
```bash
php bin/urlhasher http://speed.transip.nl/10mb.bin --algo sha256
```
Custom algorithm example:
```bash
php bin/urlhasher http://speed.transip.nl/10mb.bin --algo complex
```

### Tests
You can run the tests with the following command:
```bash
./vendor/bin/phpunit --testdox tests
```


# Bonus
- [x] Unit tests on your code
- [x] Explanation of the choices you made in a `README.md` file
- [x] Allow users to specify more than one remote file for hashing

# Explanation of the choices

> First, I want to say this was an interesting and challenging assignment for me. <br><br>
Actually, This was my second experience in implementing CLI tools. My first experience with CLI was with Laravel. I researched and learned a lot during the implementation, and I think I have to appreciate teamBlue because of this project.<br><br>
As a confession: I am not satisfied with my tests, but considering the challenge of this project for me and the lack of time, this was the best thing I could offer.<br>

## Downloader
I have two classes for downloading files in the Downloader folder: `SimpleDownloader` and `CachingDownloader`.

In SimpleDownloader I used curl to download the file from the URL and return the filename, and in `CachingDownloader` I passed `SimpleDownloader` as a constructor parameter and use that to download, But if the file exists in the downloaded file folder we just return the file.

I chose the **Proxy** design pattern because maybe we need to download and hash large file sizes multiple times, I think this is the wrong solution every time we download the file. So I decided to cache the downloaded file, and I think the Proxy pattern is the best solution for this situation Because we have a class for downloading a file from a URL, and we just need the proxy to cache the downloaded file.

## FileHasher
In `Filehasher` we need to provide different types of the hashing algorithm, I use the `hash_file()` PHP method to hash a file with the standard algorithm provided by the `hash_algos()` PHP method.

But we need to consider that maybe we need to implement our custom hashing algorithm.

Well, in this situation I decided to use the **Strategy** design pattern. With this pattern, we can switch between different algorithms in runtime. I think this pattern is the best choice for this situation.