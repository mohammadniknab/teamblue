<?php

namespace TeamBlue\Core\Downloader;

use Exception;

class SimpleDownloader extends Downloader
{

    /**
     * Starts downloading file from url
     *
     * @param string $url
     * @param ?callable $progress
     * @return string
     * @throws Exception
     */
    public function download(string $url, ?callable $progress = null): string
    {
        $filePath = $this->getFilePath($url);

        // Uses curl for download the file
        $channel = curl_init();
        $fileHandler = fopen($filePath, 'wb');

        curl_setopt_array($channel, [
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_FILE => $fileHandler,
            CURLOPT_NOPROGRESS => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_FAILONERROR => true
        ]);

        // Uses 'CURLOPT_PROGRESSFUNCTION' option to return the progress of downloading the file
        if(!is_null($progress)){
            curl_setopt($channel, CURLOPT_PROGRESSFUNCTION, function ($resource, $downloadSize, $downloaded, $uploadSize, $uploaded) use ($progress) {
                $progress($downloadSize, $downloaded);
            });
        }

        curl_exec($channel);

        if (curl_errno($channel)) {
            throw new Exception('ERROR in downloading the file');
        }

        curl_close($channel);
        fclose($fileHandler);

        return realpath($filePath);
    }


}