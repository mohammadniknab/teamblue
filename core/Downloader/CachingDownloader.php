<?php

namespace TeamBlue\Core\Downloader;

use Exception;

class CachingDownloader extends Downloader
{

    public function __construct(
        private readonly SimpleDownloader $simpleDownloader
    )
    {
        parent::__construct();
    }

    /**
     * Starts downloading file from url
     *
     * @param string $url
     * @param ?callable $progress
     * @return string
     * @throws Exception
     */
    public function download(string $url, ?callable $progress = null): string
    {
        $filePath = $this->getFilePath($url);

        if(file_exists($filePath)){
            return $filePath;
        }

        // TODO: implement real caching

        return $this->simpleDownloader->download($url, $progress);
    }
}