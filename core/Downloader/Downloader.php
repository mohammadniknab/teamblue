<?php

namespace TeamBlue\Core\Downloader;

abstract class Downloader
{
    public function __construct(
        protected readonly string $destinationPath = 'downloadedFiles/'
    )
    {
        //
    }

    /**
     * Generates file name based on file URL
     *
     * @param string $url
     * @return string
     */
    protected function getFileNameFromUrl(string $url): string
    {
        return substr(md5($url), 0, 10);
    }

    /**
     * Gets full path of file location
     *
     * @param string $url
     * @return string
     */
    protected function getFilePath(string $url): string
    {
        return $this->destinationPath . $this->getFileNameFromUrl($url);
    }

    /**
     * Starts downloading file from url
     *
     * @param string $url
     * @param ?callable $progress
     * @return string
     */
    abstract public function download(string $url, ?callable $progress = null): string;
}