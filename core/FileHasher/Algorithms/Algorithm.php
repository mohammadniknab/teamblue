<?php

namespace TeamBlue\Core\FileHasher\Algorithms;

interface Algorithm
{
    /**
     * Hashes the file
     *
     * @param string $filePath
     * @return string
     */
    public function hash(string $filePath): string;
}