<?php

namespace TeamBlue\Core\FileHasher\Algorithms;

class ComplexAlgorithm implements Algorithm
{
    /**
     * Hashes the file
     *
     * @param string $filePath
     * @return string
     */
    public function hash(string $filePath): string
    {

        /**
         *
         *  Note:
         *  This is a custom sample algorithm
         *  Implemented Just for showing how custom samples are worked
         *
         */


        $salt = sha1($filePath);
        $file = fopen($filePath, "r");
        $content = fread($file, filesize($filePath));

        $hash = hash('sha256', $content) . $salt;
        $hash = sha1($hash);

        // TODO: Implement something complex

        return md5($hash);
    }
}