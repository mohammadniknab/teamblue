<?php

namespace TeamBlue\Core\FileHasher\Algorithms;

class StandardAlgorithm implements Algorithm
{
    public function __construct(
        protected string $algorithm
    )
    {
        //
    }

    /**
     * Hashes the file
     *
     * @param string $filePath
     * @return string
     */
    public function hash(string $filePath): string
    {
        return hash_file($this->algorithm, $filePath);
    }
}