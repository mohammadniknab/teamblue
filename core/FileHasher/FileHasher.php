<?php

namespace TeamBlue\Core\FileHasher;

use TeamBlue\Core\FileHasher\Algorithms\StandardAlgorithm;

class FileHasher
{
    /**
     * Starts hashing file
     *
     * @param string $algorithm
     * @param string $filePath
     * @return string|null
     */
    static function hash(string $algorithm, string $filePath): ?string
    {
        $hash = null;

        // Starts 'StandardAlgorithm' if algorithm method is provided by PHP
        if(in_array($algorithm, hash_algos())){
            $standardAlgorithm = new StandardAlgorithm($algorithm);
            $hash = $standardAlgorithm->hash($filePath);
        }else{
            // OR, We can use our custom algorithms
            $algorithmClass = 'TeamBlue\Core\FileHasher\Algorithms\\' .ucfirst(strtolower($algorithm)) . 'Algorithm';

            if(class_exists($algorithmClass)){
                $algorithmHandler = new $algorithmClass();
                $hash = $algorithmHandler->hash($filePath);
            }
        }

        return $hash;
    }
}