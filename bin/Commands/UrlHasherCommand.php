<?php

namespace TeamBlue\Console\Commands;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TeamBlue\Core\Downloader\CachingDownloader;
use TeamBlue\Core\Downloader\SimpleDownloader;
use TeamBlue\Core\FileHasher\FileHasher;

#[AsCommand(
    name: 'urlhasher',
    description: 'File hashing with URL.',
)]
class UrlHasherCommand extends Command
{

    private InputInterface $input;
    private OutputInterface $output;
    private SymfonyStyle $io;
    private bool $cache;
    private string $algorithm;


    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setHelp('Executes hashing on a remote file.')
            ->addArgument(
                'urls',
                InputArgument::IS_ARRAY,
                'Enter the URLs (separate multiple URLs with a space)'
            )
            ->addOption(
                name: 'cache',
                mode: InputOption::VALUE_NEGATABLE,
                description: 'Enable/Disable caching in downloader',
                default: true
            )
            ->addOption(
                name: 'algo',
                shortcut: 'a',
                mode: InputOption::VALUE_OPTIONAL,
                description: 'Hashing algorithm',
                default: 'md5'
            );
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->input = $input;
        $this->output = $output;
        $this->io = new SymfonyStyle($this->input, $this->output);

        $this->io->title('..:: UrlHasher CLI tool ::..');

        // hash algorithm
        $this->algorithm = $this->input->getOption('algo');

        // cache option for downloading file
        $this->cache = $this->input->getOption('cache');


        // Gets urls arguments
        $urls = $this->getUrls();

        // Starts processing files
        $result = $this->processFiles($urls);

        $this->io->newLine();
        $this->io->note('Processing is finished');
        $this->io->newLine();

        // Prints results
        $this->io->text('Results:');
        $this->io->table(
            [
                '<fg=yellow;options=bold> File </>',
                '<fg=yellow;options=bold> Hash Code </>'
            ],
            array_map(
                null,
                array_keys($result['hashedUrls']),
                array_values($result['hashedUrls'])
            )
        );

        // Prints summary
        $this->io->info(sprintf('Total files: %s, Success: %s, Failed: %s', count($urls), $result['successfulCount'], $result['failedCount']));

        // Asks from the user for confirmation about retry hashing when errors occurred
        if (!!$result['failedCount'] && $this->confirmForRetry()) {
            $this->processFiles($urls);
        }

        return Command::SUCCESS;
    }

    /**
     * Gets URLs from arguments or asks the user
     *
     * @return array
     */
    private function getUrls(): array
    {
        // Checking the URLs arguments and return that if exists
        if (!!$this->input->getArgument('urls')) {
            $urls = $this->input->getArgument('urls');
        } else {

            // URLs are taken from the user by interactive CLI
            do {
                $urls = $this->askUrlsArgument();
            } while (!$urls);
        }

        // Purging duplicate URLs
        return array_unique($urls);
    }

    /**
     * Gets urls from interaction cli
     *
     * @return array
     */
    private function askUrlsArgument(): array
    {
        $this->io->text('To start hashing remote file, you must be enter valid URL(s)!');
        $urls = $this->io->ask('Please enter the URLs (separate multiple URLs with a space)');

        if (!$urls) {
            // if the user left the URLs arguments empty we use a callback until the user fills URLs correctly
            $this->askUrlsArgument();
        }

        return explode(' ', $urls);
    }

    /**
     * Starts processing files
     *
     * @param array $urls
     * @return array
     */
    private function processFiles(array $urls): array
    {
        $hashedUrls = [];
        foreach ($urls as $index => $url) {
            $this->io->section(sprintf('Starting process file %s/%s', ++$index, count($urls)));

            // First, download file
            $file = $this->downloadFileFromUrl($url);

            // Second, starts downloaded file hash
            $hashedUrls[$url] = $this->hashingFile($file);
        }

        // Calculates summary results
        $successfulCount = count(array_filter($hashedUrls));
        $failedCount = count($urls) - $successfulCount;

        return compact('successfulCount', 'failedCount', 'hashedUrls');
    }

    /**
     * Downloads file from url
     *
     * @param string $url
     * @return string|null
     */
    private function downloadFileFromUrl(string $url): string|null
    {
        $this->io->text('Downloading file: ' . $url);
        try {
            // creates progress bar for downloading the file
            $progressBar = new ProgressBar($this->output, 100);
            $progressBar->setFormat('very_verbose');
            $progressBar->start();

            $downloader = new SimpleDownloader();

            // If the cache option is enabled, uses caching downloader
            if ($this->cache) {
                $downloader = new CachingDownloader($downloader);
            }

            // Starts download
            $fileName = $downloader->download($url, function ($download_size = 0, $downloaded = 0) use ($progressBar) {
                $percentage = $download_size > 0 && $downloaded > 0 ? ceil($downloaded * 100 / $download_size) : 0;

                if ($percentage) {
                    $progressBar->setProgress($percentage);
                }
            });

            $progressBar->finish();
            $this->io->newLine();

            // returns downloaded file name for processing hash
            return $fileName;

        } catch (\Exception $e) {
            $this->io->error('Exception: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Hashes the downloaded file
     *
     * @param string|null $file
     * @return string|null
     */
    private function hashingFile(string|null $file): string|null
    {
        if (!$file) {
            $this->io->text('<fg=yellow;options=bold>Hashing file: SKIPPED</>');
            return null;
        }

        try {
            // Starts hashing the file with algorithm option
            $hash = FileHasher::hash($this->algorithm, $file);

            $this->io->text('<fg=green;options=bold>Hashing file: DONE</>');

            return $hash;

        } catch (\Exception $e) {
            $this->io->error('Exception: ' . $e->getMessage());
            return null;
        }
    }

    /**
     * Asks from the user for confirmation about retry hashing
     *
     * @return bool
     */
    private function confirmForRetry(): bool
    {
        return $this->io->confirm('Do you want to retry?', false);
    }
}