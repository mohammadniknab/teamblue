<?php

namespace TeamBlue\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use TeamBlue\Core\Downloader\CachingDownloader;
use TeamBlue\Core\Downloader\SimpleDownloader;

final class DownloadingTest extends TestCase
{
    private string $file;

    /**
     * @throws Exception
     */
    public function testSimpleDownloader():void
    {
        $url = 'http://speed.transip.nl/10mb.bin';
        $simpleDownloader = new SimpleDownloader();
        $this->file = $simpleDownloader->download($url);

        $fileExists = file_exists($this->file);

        $this->assertTrue($fileExists);
        $this->assertIsString($this->file);
    }

    public function tearDown():void
    {
        parent::tearDown();

        unlink($this->file);
    }
}