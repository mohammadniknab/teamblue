<?php

namespace TeamBlue\Tests;

use Exception;
use PHPUnit\Framework\TestCase;
use TeamBlue\Core\Downloader\SimpleDownloader;
use TeamBlue\Core\FileHasher\FileHasher;

class HashingTest extends TestCase
{
    private string $file;

    /**
     * @throws Exception
     */
    public function setUp():void
    {
        parent::setUp();

        $simpleDownloader = new SimpleDownloader();
        $this->file = $simpleDownloader->download('http://speed.transip.nl/10mb.bin');
    }


    public function testMd5Hashing():void
    {
        $hash = FileHasher::hash('md5',$this->file);

        $this->assertIsString($hash);
        $this->assertEquals(32, strlen($hash));
    }

    public function tearDown(): void
    {
        parent::tearDown();

        unlink($this->file);
    }
}